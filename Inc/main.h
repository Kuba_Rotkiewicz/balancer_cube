/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */
uint16_t SetPWMFreq(TIM_HandleTypeDef *htim, int freq);
void decode_uart_msg();
float get_dt(int *last_time);
/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
/* USER CODE BEGIN Private defines */
#define PWM_LEFT_PIN 		5	// port B
#define DIR_LEFT_PIN 		4	// port B
#define EN_LEFT_PIN  		0	// port B

#define PWM_RIGHT_PIN 		8	// port B
#define DIR_RIGHT_PIN 		1	// port B
#define EN_RIGHT_PIN 		2	// port B

//#define PID_KP  2.0f
//#define PID_KI  0.5f
//#define PID_KD  0.25f

#define PID_TAU_LEFT 0.1f
#define PID_LIM_MIN_LEFT -28000.0f
#define PID_LIM_MAX_LEFT  28000.0f
#define PID_LIM_MIN_INT_LEFT -28000.0f
#define PID_LIM_MAX_INT_LEFT  28000.0f
#define SAMPLE_TIME_S_LEFT 0.002f

#define PID_TAU_RIGHT 0.1f
#define PID_LIM_MIN_RIGHT -28000.0f
#define PID_LIM_MAX_RIGHT  28000.0f
#define PID_LIM_MIN_INT_RIGHT -28000.0f
#define PID_LIM_MAX_INT_RIGHT 28000.0f
#define SAMPLE_TIME_S_RIGHT 0.002f

//#define POSITION

#ifdef POSITION
#define PID_TAU_POS_LEFT 0.1f
#define PID_LIM_MIN_POS_LEFT -4.0f
#define PID_LIM_MAX_POS_LEFT  4.0f
#define PID_LIM_MIN_INT_POS_LEFT -1.0f
#define PID_LIM_MAX_INT_POS_LEFT  1.0f
#define SAMPLE_TIME_S_POS_LEFT 0.002f

#define PID_TAU_POS_RIGHT 0.1f
#define PID_LIM_MIN_POS_RIGHT -4.0f
#define PID_LIM_MAX_POS_RIGHT  4.0f
#define PID_LIM_MIN_INT_POS_RIGHT -1.0f
#define PID_LIM_MAX_INT_POS_RIGHT  1.0f
#define SAMPLE_TIME_S_POS_RIGHT 0.002f

#define POSITION_FILTER_WINDOW_SIZE	5
#endif

#define CLOCK_CYCLES_PER_SECOND  100000000
#define MAX_RELOAD               0xFFFF

#define T_MEASUREMENT	5 // (T=5ms -> f=200Hz) Time period in ms for reading measurements from IMU
#define T_PID			2 // (T=2ms -> f=500Hz) Time period in ms for triggering pid calculations
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
