/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "dma.h"
#include "i2c.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "mpu6050.h"
#include "stdio.h"
#include "kalman.h"
#include "pid.h"
#include "stdlib.h"
#include <string.h>
#include "math.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define LEFT_ENABLE			GPIOB->ODR	&=~	(1<<EN_LEFT_PIN)
#define LEFT_DISABLE		GPIOB->ODR	|=	(1<<EN_LEFT_PIN)
#define RIGHT_ENABLE		GPIOB->ODR	&=~	(1<<EN_RIGHT_PIN)
#define RIGHT_DISABLE		GPIOB->ODR	|=	(1<<EN_RIGHT_PIN)

#define LEFT_BACKWARD 		GPIOB->ODR	&=~	(1<<DIR_LEFT_PIN)
#define LEFT_FORWARD		GPIOB->ODR	|=	(1<<DIR_LEFT_PIN)
#define RIGHT_FORWARD 		GPIOB->ODR	&=~	(1<<DIR_RIGHT_PIN)
#define RIGHT_BACKWARD		GPIOB->ODR	|=	(1<<DIR_RIGHT_PIN)

// Initial angle calibration
#define MAX_DATA 			150
#define DATA_SAMPLES		50

// Choose filter
#define KALMAN
//#define COMPLEMENTARY

//#define IMU_CALIBRATION

// Moving average accelerometer filter
#define FILTER_WINDOW_SIZE	22

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

volatile int count_seconds_dt = 0;
volatile int switch_counter = 0;

float pitch_angle = 0;
float y_angular_velocity = 0;
float dt = 0;
float dt_pid = 0;
float angle = 0;

#ifdef COMPLEMENTARY
float alpha = 0.02;
#endif

uint8_t Received[50];
uint8_t received_msg_buffer[50];
volatile int receive_UART_msg_trigger = 0;
char acc_gyr_dt_ang[50];

int offset = 0;
volatile int pid_out_control = 0;
volatile float KP = 2030.0;
volatile float KI = 0.0;
volatile float KD = 190.0;

#ifdef COMPLEMENTARY
volatile float A = 0.02;		//Complementary A=0.02
volatile float B = 0.0;			//Complementary B=40
volatile float R = 0.0;			//Complementary R=0
#endif

#ifdef KALMAN
volatile float A = 0.0001;		//Kalman article A=0.001	/	Kalman TKJ A=0.001
volatile float B = 0.00003;		//Kalman article B=0.00003	/	Kalman TKJ B=0.003
volatile float R = 0.2;		//Kalman article R=0.2		/	Kalman TKJ R=0.03
#endif

#ifdef ENCODER
volatile int16_t encoder_raw = 0;
volatile float encoder_angle = 0;
#endif


#ifdef POSITION
volatile float Kp_pos_left = 0.5;
volatile float Ki_pos_left = 0.0;
volatile float Kd_pos_left = 0.0;
volatile float Kp_pos_right = 0.5;
volatile float Ki_pos_right = 0.0;
volatile float Kd_pos_right = 0.0;
volatile float position_left = 0;
volatile float position_right = 0;
#endif
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* printf function to ITM
 int _write(int file, char *ptr, int len)
 {
 // Implement your write code here, this is used by puts and printf for example
 int i=0;
 for(i=0 ; i<len ; i++)
 ITM_SendChar((*ptr++));
 return len;
 }*/

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
#ifdef KALMAN
	Kalman k;

	k.Q_angle = A;
	k.Q_bias = B;
	k.R_measure = R;

	k.angle = 0.0; 			// Reset the angle
	k.bias = 0.0; 			// Reset bias
	k.rate = 0.0;

	k.P[0][0] = 0.0; // Since we assume that the bias is 0 and we know the starting angle (use setAngle), the error covariance matrix is set like so - see: http://en.wikipedia.org/wiki/Kalman_filter#Example_application.2C_technical
	k.P[0][1] = 0.0;
	k.P[1][0] = 0.0;
	k.P[1][1] = 0.0;

#endif

	float KP_old = 0;
	float KI_old = 0;
	float KD_old = 0;

	float Kp_left = KP;
	float Ki_left = KI;
	float Kd_left = KD;

	float Kp_right = KP;
	float Ki_right = KI;
	float Kd_right = KD;

	int pid_out_left = 0;
	int pid_out_right = 0;
	int fall_trigger = 0;

uint16_t freq_left_wheel = 0;
uint16_t freq_right_wheel = 0;

#ifdef POSITION
	float move_distance = 0;
	float position_left_array[POSITION_FILTER_WINDOW_SIZE];
	float position_right_array[POSITION_FILTER_WINDOW_SIZE];
	memset(position_left_array, 0, sizeof position_left_array);
	memset(position_right_array, 0, sizeof position_right_array);
	float position_left_sum = 0;
	float position_right_sum = 0;
#endif
	float A_old = 0.0;
	float B_old = 0.0;
	float R_old = 0.0;

	// Calibration
	float data[MAX_DATA];
	float angle_setpoint = 0;
	int angle_calib_counter = 0;

#ifdef IMU_CALIBRATION
	int16_t ax_offset;
	int16_t ay_offset;
	int16_t az_offset;
	int16_t gx_offset;
	int16_t gy_offset;
	int16_t gz_offset;
#endif

	// Measure dt for angle and dt_pid for pid
	int last_time_angle = 0;
	int last_time_pid = 0;
	// Accelerometer filter
	int filter_counter;
	float pitch_ang_sum = 0;
	float pitch_angle_f[FILTER_WINDOW_SIZE];
	memset(pitch_angle_f, 0, sizeof pitch_angle_f);

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_TIM9_Init();
  MX_TIM3_Init();
  MX_TIM4_Init();
  MX_I2C2_Init();
  MX_USART1_UART_Init();
  /* USER CODE BEGIN 2 */

	MPU6050_Init(&hi2c2);
	MPU6050_SetXAccelOffset(-2587);
	MPU6050_SetYAccelOffset(1015);
	MPU6050_SetZAccelOffset(705);
	MPU6050_SetXGyroOffset(34);
	MPU6050_SetYGyroOffset(16);
	MPU6050_SetZGyroOffset(-7);
#ifdef IMU_CALIBRATION
	ax_offset = MPU6050_GetXAccelOffset();
	ay_offset = MPU6050_GetYAccelOffset();
	az_offset = MPU6050_GetZAccelOffset();
	gx_offset = MPU6050_GetXGyroOffset();
	gy_offset = MPU6050_GetYGyroOffset();
	gz_offset = MPU6050_GetZGyroOffset();
#endif
	//MPU6050_SetInterruptMode(MPU6050_INTMODE_ACTIVEHIGH);
	//MPU6050_SetInterruptDrive(MPU6050_INTDRV_PUSHPULL);
	//MPU6050_SetInterruptLatch(MPU6050_INTLATCH_WAITCLEAR);
	//MPU6050_SetInterruptLatchClear(MPU6050_INTCLEAR_STATUSREAD);
	MPU6050_SetIntEnableRegister(0); // Disable all interrupts
#ifdef KALMAN
	// Initial values for Kalman filter - doesn't work, but not necessarry
	//MPU6050_GetPitchAngle(&k.angle);
	//MPU6050_GetYAngularVelocity(&k.rate);
#endif
	//MPU6050_SetIntDataReadyEnabled(1);

	// Timer 9 - measure time for dt calculation
	HAL_TIM_Base_Start_IT(&htim9);

	// Receive parameters over UART
	HAL_UART_Receive_DMA(&huart1, Received, 50);
	//HAL_UART_Receive_IT(&huart1, Received, 10);

	/* Initialise PID controller */
	PIDController pid_left = { Kp_left, Ki_left, Kd_left,
	PID_TAU_LEFT,
	PID_LIM_MIN_LEFT, PID_LIM_MAX_LEFT,
	PID_LIM_MIN_INT_LEFT, PID_LIM_MAX_INT_LEFT,
	SAMPLE_TIME_S_LEFT };
	PIDController_Init(&pid_left);

	/* Initialise PID controller */
	PIDController pid_right = { Kp_right, Ki_right, Kd_right,
	PID_TAU_RIGHT,
	PID_LIM_MIN_RIGHT, PID_LIM_MAX_RIGHT,
	PID_LIM_MIN_INT_RIGHT, PID_LIM_MAX_INT_RIGHT,
	SAMPLE_TIME_S_RIGHT };
	PIDController_Init(&pid_right);

#ifdef POSITION
	/* Initialise position PID controller */
	PIDController pid_position_left = { Kp_pos_left, Ki_pos_left, Kd_pos_left,
	PID_TAU_POS_LEFT,
	PID_LIM_MIN_POS_LEFT, PID_LIM_MAX_POS_LEFT,
	PID_LIM_MIN_INT_POS_LEFT, PID_LIM_MAX_INT_POS_LEFT,
	SAMPLE_TIME_S_POS_LEFT };
	PIDController_Init(&pid_position_left);

	/* Initialise position PID controller */
	PIDController pid_position_right = { Kp_pos_right, Ki_pos_right, Kd_pos_right,
	PID_TAU_POS_RIGHT,
	PID_LIM_MIN_POS_RIGHT, PID_LIM_MAX_POS_RIGHT,
	PID_LIM_MIN_INT_POS_RIGHT, PID_LIM_MAX_INT_POS_RIGHT,
	SAMPLE_TIME_S_POS_RIGHT };
	PIDController_Init(&pid_position_right);
#endif

	// Set motors direction and disable
	LEFT_FORWARD;
	RIGHT_FORWARD;
	LEFT_DISABLE;
	RIGHT_DISABLE;

	HAL_SYSTICK_Config(SystemCoreClock / 1000);
#ifdef ENCODER
	HAL_TIM_Encoder_Start(&htim1, TIM_CHANNEL_ALL);
#endif
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
	while (1) {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
		switch (switch_counter) {
		case 1:
			switch_counter = 0;
			if (angle_calib_counter == MAX_DATA) {
				// Calculating the set-point; neglect first 100 measurements
				int i = 0;
				for (i = MAX_DATA - DATA_SAMPLES; i < MAX_DATA; i++) {
					angle_setpoint = angle_setpoint + data[i];
				}
				angle_setpoint = angle_setpoint / DATA_SAMPLES;
				angle_calib_counter++;

				// Enable and run motors
				LEFT_ENABLE;
				RIGHT_ENABLE;
				HAL_TIMEx_PWMN_Start(&htim3, 2); // 2 -> TIM_CHANNEL_2
				HAL_TIMEx_PWMN_Start(&htim4, 6); // 6 -> TIM_CHANNEL_3
#ifdef POSITION
				position_left = 0;
				position_right = 0;

				pid_position_left.limMin = angle_setpoint + PID_LIM_MIN_POS_LEFT;
				pid_position_left.limMax = angle_setpoint + PID_LIM_MAX_POS_LEFT;
				pid_position_left.limMinInt = angle_setpoint + PID_LIM_MIN_INT_POS_LEFT;
				pid_position_left.limMaxInt = angle_setpoint + PID_LIM_MAX_INT_POS_LEFT;
				pid_position_left.limMin = angle_setpoint + PID_LIM_MIN_POS_RIGHT;
				pid_position_right.limMax = angle_setpoint + PID_LIM_MAX_POS_RIGHT;
				pid_position_right.limMinInt = angle_setpoint + PID_LIM_MIN_INT_POS_RIGHT;
				pid_position_right.limMaxInt = angle_setpoint + PID_LIM_MAX_INT_POS_RIGHT;
#endif
			}

			//__disable_irq();
			MPU6050_GetPitchAngle(&pitch_angle);
			MPU6050_GetYAngularVelocity(&y_angular_velocity);
			//__enable_irq();

			if(isfinite(pitch_angle) && isfinite(y_angular_velocity)){
				for(filter_counter=0;filter_counter<FILTER_WINDOW_SIZE-1;filter_counter++){
					pitch_angle_f[FILTER_WINDOW_SIZE-1-filter_counter] = pitch_angle_f[FILTER_WINDOW_SIZE-2-filter_counter];
					pitch_ang_sum += pitch_angle_f[FILTER_WINDOW_SIZE-1-filter_counter];
				}
				pitch_angle_f[0] = pitch_angle;
				pitch_angle = (pitch_ang_sum + pitch_angle_f[0])/FILTER_WINDOW_SIZE;
				pitch_ang_sum = 0;

			    y_angular_velocity = y_angular_velocity * -1;

			    dt = get_dt(&last_time_angle);

#ifdef COMPLEMENTARY
			    angle = (1 - alpha) * (angle + y_angular_velocity * dt)
					+ (alpha) * (pitch_angle);
#endif
#ifdef KALMAN
			angle = getAngle(&k, pitch_angle, y_angular_velocity, dt);
			//printf("Angle: %.2f\n", angle);
#endif

#ifdef ENCODER
			encoder_raw = htim1.Instance->CNT;
			encoder_angle = (float) encoder_raw * 180 / 1200;
			sprintf(acc_gyr_dt_ang, " %f,%f,%f,%f\n\r",pitch_angle,y_angular_velocity,dt,encoder_angle);
			HAL_UART_Transmit_DMA(&huart1, acc_gyr_dt_ang, sizeof acc_gyr_dt_ang);
#endif

				// Calibration
				if (angle_calib_counter < MAX_DATA) {
					data[angle_calib_counter] = angle;
					angle_calib_counter++;
				}
			}
			break;
		case 2:
			switch_counter = 0;
			if (receive_UART_msg_trigger != 0) {
				decode_uart_msg(&KP, &KI, &KD, &A, &B, &R, received_msg_buffer);
				memset(received_msg_buffer, 0, sizeof received_msg_buffer);

				if ((KP != KP_old) | (KI != KI_old) | (KD != KD_old)) {
					pid_left.Kp = KP;
					pid_left.Ki = KI;
					pid_left.Kd = KD;
					pid_right.Kp = KP;
					pid_right.Ki = KI;
					pid_right.Kd = KD;
					KP_old = KP;
					KI_old = KI;
					KD_old = KD;
					PIDController_Init(&pid_left);
					PIDController_Init(&pid_right);
				}

				if ((A != A_old) | (B != B_old)
						| (R != R_old)) {
#ifdef KALMAN
					setQangle(&k, A);
					setQbias(&k, B);
					setRmeasure(&k, R);
#endif
#ifdef COMPLEMENTARY
					alpha = A;
					offset = B;
#ifdef POSITION
					move_distance = R;
#endif
#endif
					A_old = A;
					B_old = B;
					R_old = R;
				}
				receive_UART_msg_trigger = 0;
			}

			dt_pid = get_dt(&last_time_pid);
#ifdef POSITION
			PIDController_Update(&pid_position_left, move_distance, position_left);
			PIDController_Update(&pid_position_right, move_distance, position_right);
			PIDController_Update(&pid_left, angle_setpoint + (float)pid_position_left.out, angle);
			PIDController_Update(&pid_right, angle_setpoint + (float)pid_position_right.out, angle);
#else
			PIDController_Update(&pid_left, angle_setpoint, angle);
			PIDController_Update(&pid_right, angle_setpoint, angle);
#endif
			pid_out_left = (int) pid_left.out;
			pid_out_right = (int) pid_right.out;
			//pid_out_control = pid_out_left;

			if (angle > angle_setpoint) {
				if (fall_trigger == 0) {
					LEFT_ENABLE;
					RIGHT_ENABLE;
#ifdef POSITION
					position_left = 0;
					position_right = 0;
#endif
					fall_trigger = 1;
				}
				freq_left_wheel = SetPWMFreq(&htim3, abs(pid_out_left) + offset);
				freq_right_wheel = SetPWMFreq(&htim4, abs(pid_out_right) + offset);
				LEFT_FORWARD;
				RIGHT_FORWARD;
#ifdef POSITION
				position_left = position_left + (float)freq_left_wheel * dt_pid * 0.00800124378961150465420954486679;
				position_right = position_right + (float)freq_right_wheel * dt_pid * 0.00800124378961150465420954486679;

				for(filter_counter=0;filter_counter<POSITION_FILTER_WINDOW_SIZE-1;filter_counter++){
					position_left_array[POSITION_FILTER_WINDOW_SIZE-1-filter_counter] = position_left_array[POSITION_FILTER_WINDOW_SIZE-2-filter_counter];
					position_left_sum += position_left_array[POSITION_FILTER_WINDOW_SIZE-1-filter_counter];

					position_right_array[POSITION_FILTER_WINDOW_SIZE-1-filter_counter] = position_right_array[POSITION_FILTER_WINDOW_SIZE-2-filter_counter];
					position_right_sum += position_right_array[POSITION_FILTER_WINDOW_SIZE-1-filter_counter];
				}
				position_left_array[0] = position_left;
				position_left = (position_left_sum + position_left_array[0])/POSITION_FILTER_WINDOW_SIZE;
				position_left_sum = 0;

				position_right_array[0] = position_right;
				position_right = (position_right_sum + position_right_array[0])/POSITION_FILTER_WINDOW_SIZE;
				position_right_sum = 0;
#endif

			} else if (angle < angle_setpoint) {
				if (fall_trigger == 0) {
					LEFT_ENABLE;
					RIGHT_ENABLE;
					fall_trigger = 1;
				}
				freq_left_wheel = SetPWMFreq(&htim3, abs(pid_out_left) + offset);
				freq_right_wheel = SetPWMFreq(&htim4, abs(pid_out_right) + offset);
				LEFT_BACKWARD;
				RIGHT_BACKWARD;
#ifdef POSITION
				position_left = position_left - (float)freq_left_wheel * dt_pid * 0.00800124378961150465420954486679;
				position_right = position_right - (float)freq_right_wheel * dt_pid * 0.00800124378961150465420954486679;

				for(filter_counter=0;filter_counter<POSITION_FILTER_WINDOW_SIZE-1;filter_counter++){
					position_left_array[POSITION_FILTER_WINDOW_SIZE-1-filter_counter] = position_left_array[POSITION_FILTER_WINDOW_SIZE-2-filter_counter];
					position_left_sum += position_left_array[POSITION_FILTER_WINDOW_SIZE-1-filter_counter];

					position_right_array[POSITION_FILTER_WINDOW_SIZE-1-filter_counter] = position_right_array[POSITION_FILTER_WINDOW_SIZE-2-filter_counter];
					position_right_sum += position_right_array[POSITION_FILTER_WINDOW_SIZE-1-filter_counter];
				}
				position_left_array[0] = position_left;
				position_left = (position_left_sum + position_left_array[0])/POSITION_FILTER_WINDOW_SIZE;
				position_left_sum = 0;

				position_right_array[0] = position_right;
				position_right = (position_right_sum + position_right_array[0])/POSITION_FILTER_WINDOW_SIZE;
				position_right_sum = 0;
#endif
			}
			if (angle < angle_setpoint - 70 || angle > angle_setpoint + 70) {
				LEFT_DISABLE;
				RIGHT_DISABLE;
				fall_trigger = 0;
			}

			break;
		default:
			break;
		}
		// }
	}
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 100;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_3) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/**
 * @brief EXTI Interrupt Handler - read MPU-6050 values
 * @retval None
 */
/*void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
 {
 if(switch_counter != 1){
 switch_counter = 1;
 }
 }*/

/**
 * @brief TIM9 Interrupt Handler - time counter for dt
 * @retval None
 */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim) {
}

/**
 * @brief Rx received callback
 * @retval None
 */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {
	int i;
	uint8_t Data[60]; 				// Tablica przechowujaca wysylana wiadomosc.
	memset(Data, 0, sizeof Data);

	sprintf((char*) Data, "OK: %s", Received);
	HAL_UART_Transmit_DMA(&huart1, Data, sizeof Data); // Rozpoczecie nadawania danych z wykorzystaniem przerwan

	for (i = 0; i < (sizeof Received); i++) {
		received_msg_buffer[i] = Received[i];
	}
	memset(Received, 0, sizeof Received);
	HAL_UART_Receive_DMA(&huart1, Received, sizeof Received); // Ponowne włączenie nasłuchiwania
	receive_UART_msg_trigger = 1;
}

/**
 * @brief Set frequency of PWM signal in TIM3
 * @retval None
 */
uint16_t SetPWMFreq(TIM_HandleTypeDef *htim, int freq) {

	if (freq > 0 && freq <= 30000
			&& (htim->Instance == TIM3 || htim->Instance == TIM4)) {
		__disable_irq();
		uint32_t period_cycles = CLOCK_CYCLES_PER_SECOND / freq;
		uint16_t prescaler = (uint16_t) (period_cycles / MAX_RELOAD + 1);
		uint16_t overflow = (uint16_t) ((period_cycles + (prescaler / 2))
				/ prescaler);
		uint16_t duty = (uint16_t) (overflow / 2);

		__HAL_TIM_SET_AUTORELOAD(htim, overflow);
		__HAL_TIM_SET_PRESCALER(htim, prescaler);
		/* Generate an update event to reload the Prescaler */
		if (htim->Instance == TIM3) {
			TIM3->EGR |= TIM_EGR_UG;
			__HAL_TIM_SET_COMPARE(htim, TIM_CHANNEL_2, duty);
		} else if (htim->Instance == TIM4) {
			TIM4->EGR |= TIM_EGR_UG;
			__HAL_TIM_SET_COMPARE(htim, TIM_CHANNEL_3, duty);
		}
		__enable_irq();

		return (uint16_t)(CLOCK_CYCLES_PER_SECOND/((overflow+1)*(prescaler+1)));
	}

	return 0;
}
/**
 * @brief Decode uart message
 * @retval None
 */
void decode_uart_msg(float *kp, float *ki, float *kd, float *ka, float *kb,
		float *kr, uint8_t *recived_message) {
	char buffer[6][7];
	float p, i, d, a, b, r;

	if (recived_message[0] == 'p') {
		int j;
		for (j = 1; j < 8; j++) {
			buffer[0][j - 1] = recived_message[j];
			buffer[1][j - 1] = recived_message[j + 8];
			buffer[2][j - 1] = recived_message[j + 16];
			buffer[3][j - 1] = recived_message[j + 24];
			buffer[4][j - 1] = recived_message[j + 32];
			buffer[5][j - 1] = recived_message[j + 40];
		}
		p = atof(&buffer[0][0]);
		i = atof(&buffer[1][0]);
		d = atof(&buffer[2][0]);
		a = atof(&buffer[3][0]);
		b = atof(&buffer[4][0]);
		r = atof(&buffer[5][0]);

		if (p >= 0 && p < 4000) {
			*kp = p;
		}
		if (i >= 0 && i < 2000) {
			*ki = i;
		}
		if (d >= 0 && d < 2000) {
			*kd = d;
		}
		if (a >= 0 && a < 100) {
			*ka = a;
		}
		if (b >= 0 && b < 600) {
			*kb = b;
		}
		if (r >= 0 && r < 100) {
			*kr = r;
		}
	}
}

/**
 * @brief Get dt
 * @param *last_time
 * @retval dt
 */
float get_dt(int *last_time) {
	int current_time;
	float dt;
	current_time = TIM9->CNT;
	if (current_time > *last_time) {
		dt = ((float) current_time - (float) *last_time) / 19999;
	} else {
		dt = ((float) 19999 - (float) *last_time + (float) current_time)
				/ 19999;
	}
	*last_time = current_time;
	return dt;
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
	/* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
