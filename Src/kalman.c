#include "kalman.h"

// The angle should be in degrees and the rate should be in degrees per second and the delta time in seconds
float getAngle(Kalman* k, float newAngle, float newRate, float dt) {
    // Modified by Kristian Lauszus
    // See my blog post for more information: http://blog.tkjelectronics.dk/2012/09/a-practical-approach-to-kalman-filter-and-how-to-implement-it

    // Discrete Kalman filter time update equations - Time Update ("Predict")
    // Update xhat - Project the state ahead
    /* Step 1 */
	k->rate = newRate - k->bias;
	k->angle += dt * k->rate;

    // Update estimation error covariance - Project the error covariance ahead
    /* Step 2 */
	k->P[0][0] += dt * (dt*k->P[1][1] - k->P[0][1] - k->P[1][0] + k->Q_angle);
	k->P[0][1] -= dt * k->P[1][1];
	k->P[1][0] -= dt * k->P[1][1];
	k->P[1][1] += k->Q_bias * dt;

    // Discrete Kalman filter measurement update equations - Measurement Update ("Correct")
    // Calculate Kalman gain - Compute the Kalman gain
    /* Step 4 */
    float S = k->P[0][0] + k->R_measure; 						// Estimate error

    /* Step 5 */
    float K[2]; // Kalman gain - This is a 2x1 vector
    K[0] = k->P[0][0] / S;
    K[1] = k->P[1][0] / S;

    // Calculate angle and bias - Update estimate with measurement zk (newAngle)
    /* Step 3 */
    float y = newAngle - k->angle; 								// Angle difference

    /* Step 6 */
    k->angle += K[0] * y;
    k->bias += K[1] * y;

    // Calculate estimation error covariance - Update the error covariance
    /* Step 7 */
    float P00_temp = k->P[0][0];
    float P01_temp = k->P[0][1];

    k->P[0][0] -= K[0] * P00_temp;
    k->P[0][1] -= K[0] * P01_temp;
    k->P[1][0] -= K[1] * P00_temp;
    k->P[1][1] -= K[1] * P01_temp;

    return k->angle;
};

void setAngle(Kalman* k, float angle) { k->angle = angle; }; 	// Used to set angle, this should be set as the starting angle
float getRate(Kalman* k) { return k->rate; }; 					// Return the unbiased rate

/* These are used to tune the Kalman filter */
void setQangle(Kalman* k, float Q_angle) { k->Q_angle = Q_angle; };
void setQbias(Kalman* k, float Q_bias) { k->Q_bias = Q_bias; };
void setRmeasure(Kalman* k, float R_measure) { k->R_measure = R_measure; };

float getQangle(Kalman* k) { return k->Q_angle; };
float getQbias(Kalman* k) { return k->Q_bias; };
float getRmeasure(Kalman* k) { return k->R_measure; };
