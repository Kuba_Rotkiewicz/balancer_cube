# STM32 based Self Balancing Robot
STM32 based project of two-wheel self balancing robot for further development of different control algorithms.
Next version of the robot is based on Raspberry Pi.
![Self Balancing Robot](Images/balancer1.jpg)

## Components: 
* NUCLEO STM32F411RE
* 2x A4899 Stepper motor drivers
* 2x NEMA 17 Stepper motors
* 7805 Voltage Regulator 
* Battery Management System
* 3S1P Li-Ion Battery Pack
* Bi-Directional Logic Level Converter (3.3V - 5V)
* MPU6050 IMU
* 4x M4 rods, 16x washers, 16x nuts
* 2x 170x70x3mm plywoods
* Bluetooth Module HC-05

# Features
* PID Controller - [YouTube](https://youtu.be/zOByx3Izf5U)
* Kalman Filter - rewritten in C, original: [TKJ Electronics](https://blog.tkjelectronics.dk/2012/09/a-practical-approach-to-kalman-filter-and-how-to-implement-it/)
* Complementary Filter 
* App for remote online tuning PID and Kalman filter parameters - [MIT App Inventor](http://appinventor.mit.edu/)

# Documents
Document concerning the design is out of date but still might be helpful. The robot is capable of balancing now.